const axios = require("axios");

const EditPhotoHandler = async (text, msg) => {
  const cmd = text.split("/"); // ["edit_bg", "bg_color"]
  if (cmd.length < 2) {
    return msg.reply("Format Salah. gini ketiknya *edit_bg/warna*");
  }

  if (msg.hasMedia) {
    if (msg.type != "image") {
      return msg.reply("hanya bisa edit dengan format image.");
    }

    msg.reply("sedang diproses, sabar bentar...");

    const media = await msg.downloadMedia();

    if (media) {
      const color = cmd[1];
      const newPhoto = await EditPhotoRequest(media.data, color);

      if (!newPhoto.success) {
        return msg.reply("Terjadi kesalahan.");
      }

      const chat = await msg.getChat();
      media.data = newPhoto.base64;
      chat.sendMessage(media, {
        caption: "ini nah \n jangan lupa follow @ricky_rd05 😉",
      });
    }
  }
};

const EditPhotoRequest = async (base64, bg_color) => {
  const result = {
    success: false,
    base64: null,
    message: "",
  };

  return await axios({
    method: "post",
    url: "https://api.remove.bg/v1.0/removebg",
    data: {
      image_file_b64: base64,
      bg_color: bg_color,
    },
    headers: {
      accept: "application/json",
      "Content-Type": "application/json",
      "X-Api-Key": "LELyuoGzpacxyesLfENTFEid",
    },
  })
    .then((response) => {
      if (response.status == 200) {
        result.success = true;
        result.base64 = response.data.data.result_b64;
      } else {
        result.message = "Failed response";
      }

      return result;
    })
    .catch((error) => {
      result.message = "Error : " + error.message;
      return result;
    });
};

module.exports = {
  EditPhotoHandler,
};
