const qrcode = require("qrcode-terminal");
const express = require("express");
// const session = require("express-session");
const MongoStoreMongo = require("connect-mongo");
const { Client, RemoteAuth, LocalAuth } = require("whatsapp-web.js");
const { MongoStore } = require("wwebjs-mongo");
const mongoose = require("mongoose");
const { EditPhotoHandler } = require("./feature/edit_foto");
const { ChatAIHandler } = require("./feature/chat_ai");
const dotenv = require("dotenv");

dotenv.config();

const app = express();
const server = require("http").createServer(app);
const PORT = process.env.PORT || 3001;

// mongoose.set("strictQuery", true);

// mongoose
//   .connect(process.env.MONGODB_URI)
//   .then(async () => {
//     console.log("berhasil terhubung");
//     const store = new MongoStore({ mongoose: mongoose });

// const client = new Client({
//   authStrategy: new RemoteAuth({
//     store: store,
//     backupSyncIntervalMs: 300000,
//   }),
// });
// await store.save({ session: "yourSessionName" });

const client = new Client({
  authStrategy: new LocalAuth(),
  puppeteer: { headless: false },
});

client.initialize();

client.on("qr", (qr) => {
  qrcode.generate(qr, { small: true });
});

client.on("change_state", (state) => {
  console.log("CHANGE STATE", state);
});

client.on("disconnected", (reason) => {
  console.log("Client was logged out", reason);
});

client.on("auth_failure", (msg) => {
  // Fired if session restore was unsuccessful
  console.error("AUTHENTICATION FAILURE", msg);
});

client.on("ready", () => {
  console.log("Client is ready!");
});

client.on("message", async (msg) => {
  console.log("message", msg.body);
  const { body } = msg;

  if (
    body.toLowerCase() === "!ping" ||
    body.toLowerCase() === "test" ||
    body.toLowerCase() === "p"
  ) {
    msg.reply(
      "'ask/pertanyaan' mu => untuk bertanya apapun.\n'edit_bg/warna' => untuk mengubah background foto(foto dan text keyword tidak boleh terpisah).\nDon't forget to follow @ricky_rd05"
    );
  }

  //Edit background
  if (body.toLowerCase().includes("edit_bg/")) {
    await EditPhotoHandler(body.toLowerCase(), msg);
  }

  //openAi
  if (body.toLowerCase().includes("ask/")) {
    await ChatAIHandler(body.toLowerCase(), msg);
  }
});

// })
// .catch((err) => {
//   console.log("gagal terhubung");
//   console.log(err);
// });
//-------------------------------------------------------

// client.on("qr", (qr) => {
//   qrcode.generate(qr, { small: true });
// });

// client.on("ready", () => {
//   console.log("Client is ready!");
// });

// client.on("message", async (msg) => {
//   const { body } = msg;
//   console.log(body);

//   if (body.toLowerCase() === "!ping") {
//     msg.reply("Don't forget to follow @ricky_rd05");
//   }

//   //Edit background
//   if (body.toLowerCase().includes("edit_bg/")) {
//     await EditPhotoHandler(body.toLowerCase(), msg);
//   }

//   //openAi
//   if (body.toLowerCase().includes("ask/")) {
//     await ChatAIHandler(body.toLowerCase(), msg);
//   }
// });

// client.initialize();

server.listen(PORT, () => {
  console.log("Server Berjalan pada Port : " + PORT);
});
